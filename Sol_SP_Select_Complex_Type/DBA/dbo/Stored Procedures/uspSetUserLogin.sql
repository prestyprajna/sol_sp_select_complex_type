﻿CREATE PROCEDURE [dbo].[uspSetUserLogin]
	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,
	@Username Varchar(50)=NULL,
	@Password Varchar(50)=NULL
AS
	BEGIN

		IF @Command='Insert'
		BEGIN

		DECLARE @ErrorMessage VARCHAR(MAX)

			BEGIN TRANSACTION

			BEGIN TRY
				INSERT INTO tblUserLogin
				(
					UserId,
					UserName,
					Password
				)
				VALUES
				(
					@UserId,
					@Username,
					@Password
				)
				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH


		END

		ELSE IF @Command='Update'
		BEGIN
			
			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT 
				@Username=CASE WHEN @Username IS NULL THEN L.UserName ELSE @Username END,
				@Password=CASE WHEN @Password IS NULL THEN L.Password ELSE @Password END
					FROM tblUserLogin AS L
						WHERE L.UserId=@UserId
				

				UPDATE tblUserLogin
					SET UserName=@Username,
					Password=@Password
						WHERE UserId=@UserId					

						COMMIT TRANSACTION


			END TRY

			BEGIN CATCH
				
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

		END


		ELSE IF @Command='Delete'
		BEGIN

			BEGIN TRANSACTION
			
			BEGIN TRY

				DELETE FROM tblUserLogin 					
					WHERE UserId=@UserId

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH



		END

	END