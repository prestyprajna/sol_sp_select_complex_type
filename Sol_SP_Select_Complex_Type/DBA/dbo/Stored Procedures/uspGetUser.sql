﻿CREATE PROCEDURE [dbo].[uspGetUser]
	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,

	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,

	@Username Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@MobileNo Varchar(10)=NULL,
	@EmailId Varchar(50)=NULL,

	@Status Int=NULL out,
	@Message Varchar(50)=NULL out
AS
	BEGIN

	DECLARE @ErrorMessage VARCHAR(MAX)

	IF @Command='Select'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY
				
				SELECT U.UserId,
				U.FirstName,
				U.LastName,
				UL.Username,
				UL.Password,
				UC.MobileNo,
				UC.EmailId
					FROM tblUser AS U
						INNER JOIN
							tblUserLogin AS UL
								ON U.UserId=UL.UserId
									INNER JOIN
										tblUserCommunication AS UC
											ON U.UserId=UC.UserId

					
					SET @Status=1
					SET @Message='SELECT SUCCESSFULL'

					COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			
			SET @ErrorMessage=ERROR_MESSAGE()
			ROLLBACK TRANSACTION

			SET @Status=0
			SET @Message='SELECT EXCEPTION'

			RAISERROR(@ErrorMessage,16,1)

		END CATCH

	END

	ELSE IF @Command='Search'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY

		SELECT UL.Username,
		UL.Password
			FROM tblUserLogin AS UL
				WHERE UL.UserId IN (
							SELECT UC.UserId
								FROM tblUserCommunication AS UC
									WHERE UC.MobileNo=@MobileNo
						)	
			
					SET @Status=1
					SET @Message='SEARCH SUCCESSFULL'

					COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			
			SET @ErrorMessage=ERROR_MESSAGE()
			ROLLBACK TRANSACTION

			SET @Status=0
			SET @Message='SEARCH EXCEPTION'

			RAISERROR(@ErrorMessage,16,1)

		END CATCH

	END

	ELSE IF @Command='GetById'
	BEGIN

		BEGIN TRANSACTION

		BEGIN TRY

			SELECT U.UserId,
				U.FirstName,
				U.LastName,
				UL.Username,
				UL.Password,
				UC.MobileNo,
				UC.EmailId
					FROM tblUser AS U
						INNER JOIN
							tblUserLogin AS UL
								ON U.UserId=UL.UserId
									INNER JOIN
										tblUserCommunication AS UC
											ON U.UserId=UC.UserId
												WHERE U.UserId=@UserId
					
						
			
					SET @Status=1
					SET @Message='ID SEARCH SUCCESSFULL'

					COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			
			SET @ErrorMessage=ERROR_MESSAGE()
			ROLLBACK TRANSACTION

			SET @Status=0
			SET @Message='ID SEARCH EXCEPTION'

			RAISERROR(@ErrorMessage,16,1)

		END CATCH

	END


	END