﻿CREATE PROCEDURE [dbo].[uspSetUserCommunication]
	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,
	@MobileNo Varchar(10)=NULL,
	@EmailId Varchar(50)=NULL
AS
	BEGIN

		IF @Command='Insert'
		BEGIN

		DECLARE @ErrorMessage VARCHAR(MAX)

			BEGIN TRANSACTION

			BEGIN TRY
				INSERT INTO tblUserCommunication
				(
					UserId,
					MobileNo,
					EmailId
				)
				VALUES
				(
					@UserId,
					@MobileNo,
					@EmailId
				)
				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH


		END

		ELSE IF @Command='Update'
		BEGIN
			
			BEGIN TRANSACTION

			BEGIN TRY

				SELECT 
				@MobileNo=CASE WHEN @MobileNo IS NULL THEN C.MobileNo ELSE @MobileNo END,
				@EmailId=CASE WHEN @EmailId IS NULL THEN C.EmailId ELSE @EmailId END
					FROM tblUserCommunication AS C
						WHERE C.UserId=@UserId
				

				UPDATE tblUserCommunication
					SET MobileNo=@MobileNo,
					EmailId=@EmailId
						WHERE UserId=@UserId					

						COMMIT TRANSACTION
			

			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)


			END CATCH

		END

		ELSE IF @Command='Delete'
		BEGIN

			BEGIN TRANSACTION
			
			BEGIN TRY

				DELETE FROM tblUserCommunication 					
					WHERE UserId=@UserId

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

		END


	END