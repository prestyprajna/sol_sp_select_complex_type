﻿CREATE PROCEDURE [dbo].[uspSetUser]
	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,

	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,

	@Username Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@MobileNo Varchar(10)=NULL,
	@EmailId Varchar(50)=NULL,

	@Status Int=NULL out,
	@Message Varchar(50)=NULL out
AS
	BEGIN
		
		IF @Command='Insert'
		BEGIN

		DECLARE @ErrorMessage VARCHAR(MAX)

			BEGIN TRANSACTION

			BEGIN TRY
				INSERT INTO tblUser
				(					
					FirstName,
					LastName
				)
				VALUES
				(					
					@FirstName,
					@LastName
				)
				SET @UserId=@@IDENTITY

					EXEC uspSetUserLogin @Command,@UserId,@Username,@Password
					EXEC uspSetUserCommunication @Command,@UserId,@MobileNo,@EmailId

					SET @Status=1
					SET @Message='INSERT SUCCESSFULL'

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='INSERT NOT SUCCESSFULL'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH


		END

		ELSE IF @Command='Update'
		BEGIN
			
			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT 
				@FirstName=CASE WHEN @FirstName IS NULL THEN U.FirstName ELSE @FirstName END,
				@LastName=CASE WHEN @LastName IS NULL THEN U.LastName ELSE @LastName END
					FROM tblUser AS U
						WHERE U.UserId=@UserId
				

				UPDATE tblUser
					SET FirstName=@FirstName,
					LastName=@LastName					
						WHERE UserId=@UserId


					EXEC uspSetUserLogin @Command,@UserId,@Username,@Password

					EXEC uspSetUserCommunication @Command,@UserId,@MobileNo,@EmailId

					SET @Status=1
					SET @Message='UPDATE SUCCESSFULL'				

						COMMIT TRANSACTION


			END TRY

			BEGIN CATCH
				
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='UPDATE NOT SUCCESSFULL'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

		END

		ELSE IF @Command='Delete'
		BEGIN
			
			BEGIN TRANSACTION
			
			BEGIN TRY

				DELETE FROM tblUser 					
					WHERE UserId=@UserId


					EXEC uspSetUserLogin @Command,@UserId,@Username,@Password

					EXEC uspSetUserCommunication @Command,@UserId,@MobileNo,@EmailId

					SET @Status=1
					SET @Message='DELETE SUCCESSFULL'	

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH 
				SET @ErrorMessage=ERROR_MESSAGE()
				ROLLBACK TRANSACTION

				SET @Status=0
				SET @Message='DELETE NOT SUCCESSFULL'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH

		END

	END