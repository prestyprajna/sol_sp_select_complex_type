﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Entity.Interface
{
    public interface IUserLoginEntity
    {
        int UserId { get; set; }

        string Username { get; set; }

        string Password { get; set; }
    }
}
