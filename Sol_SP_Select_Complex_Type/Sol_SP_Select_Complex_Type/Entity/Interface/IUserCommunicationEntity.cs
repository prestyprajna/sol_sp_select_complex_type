﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Entity.Interface
{
    public interface IUserCommunicationEntity
    {
        int UserId { get; set; }

        string MobileNo { get; set; }

        string EmailId { get; set; }
    }
}
