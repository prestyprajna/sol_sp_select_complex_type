﻿using Sol_SP_Select_Complex_Type.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Entity
{
    public class UserCommunicationEntity: IUserCommunicationEntity
    {
        public int UserId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}
