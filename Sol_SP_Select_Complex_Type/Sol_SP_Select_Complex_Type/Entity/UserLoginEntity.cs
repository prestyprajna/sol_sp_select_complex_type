﻿using Sol_SP_Select_Complex_Type.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Entity
{
    public class UserLoginEntity: IUserLoginEntity
    {
        public int UserId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
