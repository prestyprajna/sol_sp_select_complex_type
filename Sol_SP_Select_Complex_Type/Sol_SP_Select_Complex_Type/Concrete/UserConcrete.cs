﻿using Sol_SP_Select_Complex_Type.Concrete.Interface;
using Sol_SP_Select_Complex_Type.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_SP_Select_Complex_Type.Entity.Interface;
using System.Data.Entity.Core.Objects;
using Sol_SP_Select_Complex_Type.Entity;

namespace Sol_SP_Select_Complex_Type.Concrete
{
    public class UserConcrete: IUserConcrete
    {
        #region  declaration

        private UserTestDBEntities db = null;

        #endregion

        #region  constructor

        public UserConcrete()
        {
            db = new UserTestDBEntities();
        }

        #endregion

        #region  public methods

        public async Task<dynamic> GetData(string command, IUserEntity entityObj, Func<uspGetUserResultSet, IUserEntity> selector , Action<int?, string> storedProcOutPara = null)
        {
            return await Task.Run(() =>
            {
                try
                {

                    ObjectParameter status = null;
                    ObjectParameter message = null;

                    var getQuery =

                    db.uspGetUser(
                        command,
                        entityObj?.UserId,
                        entityObj?.FirstName,
                        entityObj?.LastName,
                        entityObj?.userLoginEntityObj?.Username,
                        entityObj?.userLoginEntityObj?.Password,
                        entityObj?.userCommunicationEntityObj?.MobileNo,
                        entityObj?.userCommunicationEntityObj?.EmailId,
                        status = new ObjectParameter("Status", typeof(int)),
                        message = new ObjectParameter("Message", typeof(string))

                        )
                        .AsEnumerable()
                        .Select(selector).ToList();

                    storedProcOutPara(Convert.ToInt32(status.Value), Convert.ToString(message.Value));


                    return getQuery;
                }
                catch (Exception)
                {

                    throw;
                }

            });

        }


        #endregion
    }
}

