﻿using Sol_SP_Select_Complex_Type.Entity;
using Sol_SP_Select_Complex_Type.Entity.Interface;
using Sol_SP_Select_Complex_Type.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    #region  select statement

                    //IEnumerable<IUserEntity> userEntityObj = await new UserRepository().SelectData(null);

                    //foreach (IUserEntity val in userEntityObj)
                    //{
                    //    Console.WriteLine(val.UserId);
                    //    Console.WriteLine(val.FirstName);
                    //    Console.WriteLine(val.LastName);
                    //    Console.WriteLine(val.userLoginEntityObj.Username);
                    //    Console.WriteLine(val.userLoginEntityObj.Password);
                    //    Console.WriteLine(val.userCommunicationEntityObj.EmailId);
                    //    Console.WriteLine(val.userCommunicationEntityObj.MobileNo);
                    //}

                    #endregion

                    #region  search statement

                    IEnumerable<IUserEntity> userEntityObj = await new UserRepository().SearchData(new UserEntity()
                    {
                        userCommunicationEntityObj = new UserCommunicationEntity()
                        {
                            MobileNo = "8108944723"
                        }

                    });

                    foreach (IUserEntity val in userEntityObj)
                    {
                        Console.WriteLine(val.userLoginEntityObj.Username);
                        Console.WriteLine(val.userLoginEntityObj.Password);

                        Console.WriteLine(val.userCommunicationEntityObj.MobileNo);
                    }

                    #endregion

                    #region  get by id 

                    //IUserEntity userEntityObj = await new UserRepository().GetByIdData(new UserEntity()
                    //{
                    //    UserId=2
                    //});


                    //    Console.WriteLine(userEntityObj?.UserId);
                    //    Console.WriteLine(userEntityObj?.FirstName);
                    //    Console.WriteLine(userEntityObj?.LastName);
                    //    Console.WriteLine(userEntityObj?.userLoginEntityObj?.Username);
                    //    Console.WriteLine(userEntityObj?.userLoginEntityObj?.Password);
                    //    Console.WriteLine(userEntityObj?.userCommunicationEntityObj?.EmailId);
                    //    Console.WriteLine(userEntityObj?.userCommunicationEntityObj?.MobileNo);



                    #endregion
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }).Wait();

        }
    }
}
