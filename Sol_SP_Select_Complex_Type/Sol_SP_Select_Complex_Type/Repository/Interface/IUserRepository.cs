﻿using Sol_SP_Select_Complex_Type.Common_Repository;
using Sol_SP_Select_Complex_Type.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Repository.Interface
{
    public interface IUserRepository:ISelect<IUserEntity>,ISearch<IUserEntity>,IGetById<IUserEntity>
    {
    }
}
