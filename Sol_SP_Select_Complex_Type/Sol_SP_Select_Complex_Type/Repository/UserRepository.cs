﻿using Sol_SP_Select_Complex_Type.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_SP_Select_Complex_Type.Entity.Interface;
using Sol_SP_Select_Complex_Type.Concrete;
using Sol_SP_Select_Complex_Type.Entity;

namespace Sol_SP_Select_Complex_Type.Repository
{
    public class UserRepository : IUserRepository
    {
        #region  declaration

        private UserConcrete userConcreteObj = null;

        #endregion

        #region  constructor

        public UserRepository()
        {
            userConcreteObj = new UserConcrete();
        }

        #endregion

        #region  public methods

        public async Task<IEnumerable<IUserEntity>> SelectData(IUserEntity entityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                var getQuery = await userConcreteObj.GetData(
                    "Select",
                    entityObj,
                    (leUspGetUserResultSetObj) => new UserEntity()
                    {
                        UserId = (int)leUspGetUserResultSetObj?.UserId,
                        FirstName = leUspGetUserResultSetObj?.FirstName,
                        LastName = leUspGetUserResultSetObj?.LastName,
                        userLoginEntityObj = new UserLoginEntity()
                        {
                            Username = leUspGetUserResultSetObj?.Username,
                            Password = leUspGetUserResultSetObj?.Password,
                        },
                        userCommunicationEntityObj = new UserCommunicationEntity()
                        {
                            MobileNo = leUspGetUserResultSetObj?.MobileNo,
                            EmailId = leUspGetUserResultSetObj?.EmailId
                        }
                    },
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    });


                return (status == 1) ? getQuery : null;
            }
            catch (Exception)
            {
                throw;
            }       

        }

        public async Task<IEnumerable<IUserEntity>> SearchData(IUserEntity entityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                var getQuery = await userConcreteObj.GetData(
                    "Search",
                    entityObj,
                    (leUspGetUserResultSetObj) => new UserEntity()
                    {
                        //UserId = (int)leUspGetUserResultSetObj?.UserId,
                        //FirstName = leUspGetUserResultSetObj?.FirstName,
                        //LastName = leUspGetUserResultSetObj?.LastName,
                        userLoginEntityObj = new UserLoginEntity()
                        {
                            Username = leUspGetUserResultSetObj?.Username,
                            Password = leUspGetUserResultSetObj?.Password,
                        },
                        userCommunicationEntityObj = new UserCommunicationEntity()
                        {
                            MobileNo =leUspGetUserResultSetObj?.MobileNo
                        }
                       
                    },
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    });


                return (status == 1) ? getQuery : null;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<IUserEntity> GetByIdData(IUserEntity entityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                var getQuery = await userConcreteObj.GetData(
                    "GetById",
                    entityObj,
                    (leUspGetUserResultSetObj) => new UserEntity()
                    {
                        UserId = leUspGetUserResultSetObj?.UserId,
                        FirstName = leUspGetUserResultSetObj?.FirstName,
                        LastName = leUspGetUserResultSetObj?.LastName,
                        userLoginEntityObj = new UserLoginEntity()
                        {
                            Username = leUspGetUserResultSetObj?.Username,
                            Password = leUspGetUserResultSetObj?.Password,
                        },
                        userCommunicationEntityObj = new UserCommunicationEntity()
                        {
                            MobileNo = leUspGetUserResultSetObj?.MobileNo,
                            EmailId = leUspGetUserResultSetObj?.EmailId
                        }

                    },
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    });


                return (status == 1) ? getQuery : null;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion




    }
}
