﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Common_Repository
{
    public interface IGetDelegate<TEntityResultSet,TEntity> where TEntity: class where TEntityResultSet : class
    {
        Task<dynamic> GetData(string command, TEntity entityObj,Func<TEntityResultSet,TEntity> selector ,Action<int?, string> storedProcOutPara = null);
    }
}
