﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Common_Repository
{
    public interface IGetById<TEntity> where TEntity : class
    {
        Task<TEntity> GetByIdData(TEntity entityObj);
    }
}
