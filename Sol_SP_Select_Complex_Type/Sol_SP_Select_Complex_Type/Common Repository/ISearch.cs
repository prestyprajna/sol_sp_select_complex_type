﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_SP_Select_Complex_Type.Common_Repository
{
    public interface ISearch<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> SearchData(TEntity entityObj);
    }
}
